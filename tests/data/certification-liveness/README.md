Consider the following timeline:

  t0   A, B, C are created

  t1   A certifies B - 2/60
       B certifies C - 1/60

  t2   A certifies B (expires at t3) - 2/120
       B certifies C - 1/120

  t3   A's certification of B at t2 expires.

This results in:

t1:

```text
        o A
   2/60 |
        v
        B
   1/60 |
        v
        o
        C
```

t2:

```text
         o A
   2/120 |
         v
         B
   1/120 |
         v
         o
         C
```

t3:

```text
        o A
   2/60 |
        v
        B
   1/60 |
        v
        o
        C
```
