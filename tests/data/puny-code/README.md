This is a simple network where a User ID contains an email address
that would be normalized by puny code.


```text
           o alice
           |  2/100
           v
           o hANS@bücher.tld
           |  1/100
           v
           o carol
```
