#! /bin/bash

. gen-helper.sh --directory=puny-code ${@:+"$@"}

key alice
key 'hÄNS@bücher.tld'
key carol

certify alice -a 100 -d 2 hÄNS '<hÄNS@bücher.tld>'
certify hÄNS -a 100 -d 1 carol

finish
