This test is similar to the `multiple-userids` tests, but the two
certifications are for the same User ID and key.  This works if the
certifications have the same timestamp.

There is also an old certification, which should be ignored.

```
                    alice
       50/2    /      |  70/1   \  old and ignored
               \      |         /  120/255
                     bob
                      | 120/2
                    carol
                      | 120
                    dave
```
