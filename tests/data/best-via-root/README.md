When doing backwards propagation, we return paths from other nodes to
the target.  But, because we stop when we reach the root, a returned
path may not be optimal.  Consider:

```text
A --- 120/10 ---> B --- 120/10 ---> C --- 120/10 ---> Target
 \                                                      /
  `--- 50/10 ---> Y --- 50/10 ---> Z --- 50/10 --------'
```

When the root is B, then we'll find the path: `A -> B -> C -> Target`.
But, `A -> X -> Y -> Target` is better (it's the same length, but has
more trust).  We'll find that path when we use `A`
