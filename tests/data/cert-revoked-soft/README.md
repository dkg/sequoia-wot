If a certificate is revoked, this impacts the validity of a
certification on it and the validity of a certification that it
makes.  There are 8 scenarios:

1./2.  t1 - A, B created
       t2 - A certifies B  OR  B certifies A
       t3 - A is soft revoked
       => certification is okay

3./4.  t1 - A, B created
       t2 - A is soft revoked
       t3 - A certifies B  OR  B certifies A
       => certification is bad

5./6.  t1 - A, B created
       t2 - A certifies B  OR  B certifies A
       t3 - A is hard revoked
       => certification is bad

7./8.  t1 - A, B created
       t2 - A is hard revoked
       t3 - A certifies B  OR  B certifies A
       => certification is bad


We want to consider both B as issuer of a certification and as the
target of a certification.  When B is an interior node (i.e., a
trusted introducer), we do both.  To check them separately, we can
consider a path that is just two nodes long where either the root or
the target is revoked.  Since roots are targets are treated specially,
we also want to check when the revoked node is an interior node.

Thus, we need to also consider the
subgraph A - B and the subgraph B - D.


Consider the following timeline:

  t0   A, B, C, D are created

  t1   A certifies B - 2/120
       B certifies D - 1/60
       A certifies C - 2/30
       C certifies D - 1/120

This results in:

```text
          o A
   2/90 /   \  2/30
       v     v
       B     C
   1/60 \   / 1/120
          v
          o
          D
```

  t2   B is soft revoked

This does not change the network as the certification was made before
the soft revocation:

```text
          o A
   2/90 /   \  2/30
       v     v
       B     C
   1/60 \   / 1/120
          v
          o
          D
```

  t3   A certifies B (amount = 120)
       B certifies D (amount = 120)

Because these certifications are created after B was revoked, they
should be ignored.

```text
          o A
   2/90 /   \  2/30
       v     v
       B     C
   1/60 \   / 1/120
          v
          o
          D
```
