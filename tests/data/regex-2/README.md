A - B - C should be valid.  The regex only applies to the target.

```
       alice@some.org
              | 100/7
       bob@some.org
              | 100/7/example.org
       carol@other.org
              | 100/7
       dave@their.org
              | 100/7
       ed@example.org
```
