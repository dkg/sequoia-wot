# Sequoia Web of Trust

A Rust library for authenticating bindings between User IDs and
certificates using OpenPGP's web of trust.  This library is designed
around OpenPGP's data structures, but it does not require OpenPGP
data.  Instead, it is possible to manually describe a web of trust.

## Introduction

The [web of trust] is a decentralized trust model popularized by PGP.
It is [a superset] of [X.509], which is a hierarchical trust model,
and is the most popular trust model on the public internet today.  As
used on the public internet, however, it relies on a handful of global
[certification authorities] (CAs) who often [undermine its security].

  [web of trust]: https://en.wikipedia.org/wiki/Web_of_trust
  [a superset]: https://www.oreilly.com/library/view/beautiful-security/9780596801786/ch07.html
  [X.509]: https://de.wikipedia.org/wiki/X.509
  [certification authorities]: https://en.wikipedia.org/wiki/Certificate_authority
  [undermine its security]: https://sslmate.com/resources/certificate_authority_failures

The web of trust is more nuanced than X.509.  A user can partially
trust a CA thereby preventing a single bad actor from compromising
their security.  And those who have stronger security requirements
can use the web of trust in a completely decentralized manner.

Today, the tooling around the web of trust is primitive at best.
Many people interpret this lack of good tooling as a sign that the
web of trust is intrinsically difficult to use.  We disagree and
think that efforts like our [OpenPGP CA] provide evidence that
this is not the case.

  [OpenPGP CA]: https://openpgp-ca.org/

See the [spec] for an in-depth discussion of semantics and
implementation.

  [spec]: https://sequoia-pgp.gitlab.io/sequoia-wot

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
sequoia-wot = "0.0"
```

# License

sequoia-wot is distributed under the terms of LGPL 2.0 or later.

See [LICENSE.txt](LICENSE.txt) and [CONTRIBUTING.md](CONTRIBUTING.md)
for details.
